package edu.comillas.mibd;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;


public class DA_GetExample {
    public static void main(String[] args){
        //TODO Especificación de la configuración de HBase
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        Connection connection=null;
        Admin adm = null;
        try {
            //TODO Conectarse a la base de datos
            connection = ConnectionFactory.createConnection(conf);
            //TODO Obtener un objeto administrador
            adm = connection.getAdmin();

            Table tbl = null;
            //TODO Conectarse a la tabla 'Ejemplo1' de cada alumno
            String namespace = "aap";
            String soloTableName = "ejemplo1";
            TableName tableName = TableName.valueOf(namespace+":"+soloTableName);

            //TODO Si la tabla no existe terminar la ejecución
            if (!adm.tableExists(tableName)){
                System.exit(1);
            }
            tbl = connection.getTable(tableName);



            //Se definen las variables que se utilizarán para dar de alta las columnas
            String rowKey;
            String fam1;
            String qual1;
            String val1;

            //se obtiene toda la fila
            rowKey = "0001";
            Get get = new Get(Bytes.toBytes(rowKey));
            Result result = tbl.get(get);
            Visualizador.PrintResult(result);

            //Se obtienen datos concretos de un resultado
            byte[] nombre = result.getValue(Bytes.toBytes("dp"), Bytes.toBytes("Nombre"));
            byte[] modelo = result.getValue(Bytes.toBytes("dv"), Bytes.toBytes("Modelo"));
            System.out.println("Nombre: "+Bytes.toString(nombre) + ", modelo:"+ Bytes.toString(modelo));

            //se obtiene sólo el color y el modelo
            rowKey = "0002";
            get = new Get(Bytes.toBytes(rowKey));
            String fam = "dv";
            String qual = "Color";
            get.addColumn(Bytes.toBytes(fam), Bytes.toBytes(qual));
            qual = "Modelo";
            get.addColumn(Bytes.toBytes(fam), Bytes.toBytes(qual));

            result = tbl.get(get);
            Visualizador.PrintResult(result);

            //TODO Liberar el objeto tabla
            tbl.close();

            //TODO Cerrar la conexión con HBase
            connection.close();

        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        System.out.println("FIN");
    }




}
