package edu.comillas.mibd;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class BB_CreateTablePresplit {
    public static void main(String[] args)
    {
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        String prePathCloudera= "/home/icai/tmp/Cloudera/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        //TODO Preparación de la conexión a HBase
        Connection connection = null;
        Admin adm = null;

        try {
            //TODO Conectarse a la base de datos
            connection =  ConnectionFactory.createConnection(conf);

            //TODO Obtener un objeto administrador
            adm = connection.getAdmin();

            //TODO Modificar con el nombre definido en el ejercicio AA
            String namespace = "aap";
            //TODO Definir el nombre de la tabla que se quiere crear. Poner 'Ejemplo2'
            String soloTableName1 = namespace+":ejemplo2";
            TableName tableName1 = TableName.valueOf(soloTableName1);


            //TODO Crea un descriptor de la tabla donde se pondrá la definición de la tabla y propiedades de la tabla
            HTableDescriptor descTable1 = new HTableDescriptor(tableName1);

            //TODO Crear la definición de la CF1 'datos personales' y llamarla 'dp'
            HColumnDescriptor cf1 = new HColumnDescriptor("dp");

            //TODO Definir el versionado de la CF1 a tres
            cf1.setMaxVersions(3);

            //TODO Añadir la definicón de la CF1 a la definición de la tabla
            descTable1.addFamily(cf1);

            // particiona por 4 regiones, intervalos cerrados por abajo y abierto por arriba [0002, 0005)
            byte[][] regions = new byte[][] {
               Bytes.toBytes("0002"),
               Bytes.toBytes("0005"),
               Bytes.toBytes("0007")
            };

            //Se crea la tabla
            adm.createTable(descTable1,regions);




            //TODO Definir el nombre de la tabla que se quiere crear. Poner 'Ejemplo3'
            String soloTableName2 = "ejemplo3";
            TableName tableName2 = TableName.valueOf(soloTableName2);
            //TODO Crea un descriptor de la tabla donde se pondrá la definición de la tabla y propiedades de la tabla
            HTableDescriptor descTable2 = new HTableDescriptor(tableName2);

            //TODO Crear la definición de la CF1 'datos vehículo' y llamarla 'dv'
            HColumnDescriptor cf2 = new HColumnDescriptor("dv");
            //TODO Definir el versionado de la CF1 a tres
            cf2.setMaxVersions(3);
            //TODO Añadir la definicón de la CF1 a la definición de la tabla
            descTable2.addFamily(cf2);
            //Se crea la tabla
            adm.createTable(descTable2,Bytes.toBytes("0000"), Bytes.toBytes("0020"), 5);


            //TODO Cerrar la conexión con HBase
            connection.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("FIN");
    }

}
