package edu.comillas.mibd;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class EA_DeleteExample {
    public static void main(String[] args) {
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        Connection connection=null;
        Admin adm = null;
        try {
            connection = ConnectionFactory.createConnection(conf);
            //TODO Obtener un objeto administrador
            adm = connection.getAdmin();

            Table tbl = null;
            //TODO Conectarse a la tabla 'Ejemplo1' de cada alumno
            String namespace = "aap";
            String soloTableName = "ejemplo1";
            TableName tableName = TableName.valueOf(namespace+":"+soloTableName);

            //TODO Si la tabla no existe terminar la ejecución
            if (!adm.tableExists(tableName)){
                System.exit(1);
            }
            tbl = connection.getTable(tableName);
            //TODO Conectarse a la tabla 'Ejemplo1' de cada alumno

            String rowKey = "0003";
            //Se crea un objeto delete para borrar
            Delete delete = new Delete(Bytes.toBytes(rowKey));
            //Se borra toda la fila
            tbl.delete(delete);

            //TODO Obtener toda la fila del rowKey = "0003"


            rowKey = "0002";
            //Se crea un objeto delete para borrar
            delete = new Delete(Bytes.toBytes(rowKey));
            //Se limita el borrado a la column family 'datos personales'
            delete.addFamily(Bytes.toBytes("dp"));
            //Se borra sólo la totalidad de la cf 'dp'
            tbl.delete(delete);
            //TODO Obtener toda la fila del rowKey = "0002"


            //rowKey = "0002";
            delete = new Delete(Bytes.toBytes(rowKey));
            //Se añade el cualificador "Color"
            delete.addColumns(Bytes.toBytes("dv"),Bytes.toBytes("Color"));
            //TODO añadir el cualificador "Matricula"
            delete.addColumn(Bytes.toBytes("dv"), Bytes.toBytes("Matricula"));
            //TODO borrar el elemento de la tabla
            tbl.delete(delete);
            //TODO Obtener toda la fila del rowKey = "0002"
            Get get = new Get(Bytes.toBytes("002"));
            Visualizador.PrintResult(tbl.get(get));



            rowKey = "0001";
            //TODO crear un objeto Delete para el rowKey
            delete = new Delete(Bytes.toBytes(rowKey));
            //OJO con la diferencia addcolumn y addColumns
            delete.addColumns(Bytes.toBytes("dv"),Bytes.toBytes("Color"),50);
            //TODO borrar el elemento de la tabla
            tbl.delete(delete);
            //TODO Obtener toda la fila del rowKey = "0001
            get = new Get(Bytes.toBytes("001"));
            Visualizador.PrintResult(tbl.get(get));


            //TODO Liberar el objeto tabla
            tbl.close();

            //TODO Cerrar la conexión con HBase
            connection.close();


            //TODO volver a ejecutar las inserciones (CA y CB) y las funciones get (DA y DB) y comentar lo que se aprecia


        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        System.out.println("FIN");
    }

}
