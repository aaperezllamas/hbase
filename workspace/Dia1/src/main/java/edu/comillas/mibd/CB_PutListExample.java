package edu.comillas.mibd;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CB_PutListExample {
    public static void main(String[] args){
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        Connection connection=null;
        Admin adm = null;

        try {
            //TODO Conectarse a la base de datos
            connection = ConnectionFactory.createConnection(conf);

            //TODO Obtener un objeto administrador
            adm = connection.getAdmin();

            //TODO Modificar con el nombre definido en el ejercicio AA
            String namespace = "aap";

            //TODO Definir el nombre de la tabla que se quiere utilizar. Poner 'Ejemplo1'
            String soloTableName = "ejemplo1";

            TableName tableName = TableName.valueOf(namespace+':'+soloTableName);

            //TODO verificar que existe la tabla y si no, terminar la ejecución
            if(!adm.tableExists(tableName)){
                System.exit(1);
            }

            Table tbl = connection.getTable(tableName);
            //Se crea una lista de objetos Put
            List<Put> puts = new ArrayList<Put>();

            //Se definen las variables que se utilizarán para dar de alta las columnas
            String rowKey;
            String fam1;
            String qual1;
            String val1;

            //TODO crear un registro con rowKey "0002" para un vehículo de color "Gris", Modelo "Mercedes", Matricula "1234-AAA", Motor "Diesel" y como datos personales Nombre "Manolita Lopez"
            //TODO añadir el registro creado a la lista puts
            rowKey = "0002";
            Put put1 = new Put(Bytes.toBytes(rowKey));
            fam1 = "dv";
            qual1 = "color";
            val1 = "Gris";
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Modelo";
            val1 = "mercedes";
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Matricula";
            val1 = "1234-AAA";
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Motor";
            val1 = "diesel";
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Nombre";
            val1 = "Manolita Lopez";
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            puts.add(put1);

            //TODO crear un registro con rowKey "0003" para un vehículo de color "Amarillo", modelo "Volvo", matricula "1111-ABA", motor "Diesel" y como datos personales nombre "Lupita Hernandez"
            //TODO añadir el registro creado a la lista puts

            rowKey = "0003";
            Put put2 = new Put(Bytes.toBytes(rowKey));
            fam1 = "dv";
            qual1 = "color";
            val1 = "amarillo";
            put2.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Modelo";
            val1 = "Volvo";
            put2.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Matricula";
            val1 = "1111-ABA";
            put2.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Motor";
            val1 = "diesel";
            put2.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Nombre";
            val1 = "Lupita Hernandez";
            put2.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            puts.add(put2);

            //TODO crear un registro con rowKey "0004" para un vehículo de color "Rojo", modelo "Renault", matricula "1122-ABC", motor "Gasolina" y como datos personales nombre "Felipe Pino"
            //TODO añadir el registro creado a la lista puts

            rowKey = "0004";
            Put put3 = new Put(Bytes.toBytes(rowKey));
            fam1 = "dv";
            qual1 = "color";
            val1 = "rojo";
            put3.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Modelo";
            val1 = "renault";
            put3.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Matricula";
            val1 = "1122-ABC";
            put3.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Motor";
            val1 = "gasolina";
            put3.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            qual1 = "Nombre";
            val1 = "felipe pino";
            put3.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            puts.add(put3);


            //Se inserta la lista de registros en la tabla
            tbl.put(puts);

            //TODO Liberar el objeto tabla
            tbl.close();
            //TODO Cerrar la conexión con HBase
            connection.close();

        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        System.out.println("FIN");
    }
}
