package edu.comillas.mibd;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class CA_PutExample {
    public static void main(String[] args){
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        Connection connection=null;
        Admin adm = null;

        try {
            //TODO Conectarse a la base de datos
            connection = ConnectionFactory.createConnection(conf);

            //TODO Obtener un objeto administrador
            adm = connection.getAdmin();

            //TODO Modificar con el nombre definido en el ejercicio AA
            String namespace = "aap";

            //TODO Definir el nombre de la tabla que se quiere utilizar. Poner 'Ejemplo1'
            String soloTableName = "ejemplo1";

            TableName tableName = TableName.valueOf(namespace+':'+soloTableName);

            //Si la tabla no existe se termina la ejecución
            if(!adm.tableExists(tableName)){
                System.exit(1);
            }

            //Se recupera la tabla
            Table tbl = connection.getTable(tableName);

            //Se definen las variables que se utilizarán para dar de alta las columnas
            String rowKey;
            String fam1;
            String qual1;
            String val1;

            //Se define la rowKey
            rowKey = "0001";
            //Se crea un objeto Put con la rowKey
            Put put1 = new Put(Bytes.toBytes(rowKey));
            //Se define la columna 'Color'
            fam1 = "dv";
            qual1 = "Color";
            val1 = "rojo";
            //Se añaden las propiedades al objeto Put
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), 1, Bytes.toBytes(val1));

            //Se define la columna 'Modelo'
            qual1 = "Modelo";
            val1 = "Seat";
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), 1, Bytes.toBytes(val1));

            //TODO Definir la columna 'Matricula' con valor '0000-AAA'
            qual1 = "Matricula";
            val1 = "0000-AAA";
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));

            //TODO Definir la columna 'Motor' con valor 'Diesel'
            qual1 = "Motor";
            val1 = "Diesel";
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), Bytes.toBytes(val1));

            //TODO Definir la columna 'Nombre' de la CF='dp' con valor 'Juanito Perez'
            qual1 = "Nombre";
            val1 = "Juanito Perez";
            put1.addColumn(Bytes.toBytes("dp"), Bytes.toBytes(qual1), Bytes.toBytes(val1));


            //Se inserta el registro en la tabla
            tbl.put(put1);

            //Se libera el objeto tabla
            tbl.close();

            //TODO Cerrar la conexión con HBase
            connection.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("FIN");
    }
}

