package edu.comillas.mibd;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Table;


import java.io.IOException;

public class EB_DeleteTableExample {
    public static void main(String[] args){
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        Connection connection=null;

        try {
            //TODO Conectarse a la base de datos
            connection = ConnectionFactory.createConnection(conf);
            Admin adm = connection.getAdmin();
            //TODO Obtener un objeto administrador

            String namespace = "aap";
            String soloTableName = "ejemplo2";
            TableName tableName = TableName.valueOf(namespace+":"+soloTableName);
            //TODO Obtener la tabla Ejemplo2 y comprobar que existe

            if (!adm.tableExists(tableName)){
                System.exit(1);
            }

            //Para poder borrar una tabla tiene que estar deshabilitada
            if (adm.isTableEnabled(tableName)){
                //Se deshabilita la tabla que se quiere borrar
                adm.disableTable(tableName);
            }
            //Se borra la tabla
            adm.deleteTable(tableName);


        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        System.out.println("FIN");

    }

}
