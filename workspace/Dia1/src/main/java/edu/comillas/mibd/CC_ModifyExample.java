package edu.comillas.mibd;

import javafx.scene.control.Tab;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class CC_ModifyExample {
    public static void main(String[] args){
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        Connection connection=null;
        Admin adm = null;

        try {
            //TODO Conectarse a la base de datos
            connection = ConnectionFactory.createConnection(conf);
            //TODO Obtener un objeto administrador
            adm = connection.getAdmin();

            Table tbl = null;
            //TODO Conectarse a la tabla 'Ejemplo1' de cada alumno
            String namespace = "aap";
            String soloTableName = "ejemplo1";
            TableName tableName = TableName.valueOf(namespace+":"+soloTableName);

            //TODO Si la tabla no existe terminar la ejecución
            if (!adm.tableExists(tableName)){
                System.exit(1);
            }
            tbl = connection.getTable(tableName);
            //TODO crear una lista de objetos PUT
            ArrayList<Put> puts = new ArrayList<>();


            //Se definen las variables que se utilizarán para dar de alta las columnas
            String rowKey;
            String fam1;
            String qual1;
            String val1;

            //TODO crear un registro con rowKey "0002" para un vehículo de color "Verde", cc "2.0", como datos personales Direccion "Castellana 125". Poner versión del registro igual a 2
            //TODO anadir el registro a la lista
            rowKey = "0002";
            Put put1 = new Put(Bytes.toBytes(rowKey));
            fam1 = "dv";
            qual1 = "color";
            val1 = "verde";
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), 2,Bytes.toBytes(val1));
            qual1 = "cc";
            val1 = "2.0";
            put1.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), 2,Bytes.toBytes(val1));
            qual1 = "direccion";
            val1 = "castellana";
            put1.addColumn(Bytes.toBytes("dp"), Bytes.toBytes(qual1), Bytes.toBytes(val1));
            puts.add(put1);

            //TODO crear un registro con rowKey "0003" para un vehículo de color "Rojo", modelo "Citroen". Poner versión del registro igual a 3
            //TODO anadir el registro a la lista
            rowKey = "0003";
            Put put2 = new Put(Bytes.toBytes(rowKey));
            fam1 = "dv";
            qual1 = "color";
            val1 = "rojo";
            put2.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), 3,Bytes.toBytes(val1));
            qual1 = "modelo";
            val1 = "citroen";
            put2.addColumn(Bytes.toBytes(fam1), Bytes.toBytes(qual1), 3,Bytes.toBytes(val1));
            puts.add(put2);

            //TODO insertar los registros en la tabla
            tbl.put(puts);
            //TODO Liberar el objeto tabla
            tbl.close();
            //TODO Cerrar la conexión con HBase
            connection.close();

            //TODO eliminar la linea siguiente cuando se termine el ejercicio
            throw new IOException();

        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        System.out.println("FIN");

    }
}
