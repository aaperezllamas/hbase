package edu.comillas.mibd;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class BH_PrefixFilterExample {
    public static void main(String[] args){
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        String prePathCloudera= "/home/icai/tmp/Cloudera/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        //Preparación de la conexión a HBase
        Connection connection=null;
        Admin adm = null;

        try {
            //TODO Conectarse a la base de datos
            connection = ConnectionFactory.createConnection(conf);

            //TODO Obtener un objeto administrador
            adm = connection.getAdmin();

            Table tbl = null;
            TableName tableName = TableName.valueOf("aap:ejemplo1");
            tbl = connection.getTable(tableName);

            //TODO Crear un objeto scan
            Scan scan = new Scan();
            //se define un filtro para todas las rowKey que empiecen por 001
            Filter filter = new PrefixFilter(Bytes.toBytes("001"));
            //TODO Asociar el filtro al objeto scan
            scan.setFilter(filter);
            //TODO Obtener el resultado
            ResultScanner scanner = tbl.getScanner(scan);
            //TODO Mostrar el resultado
            Visualizador.PrintResult(scanner);
            //TODO Cerrar el resultado
            scanner.close();

            //TODO hacer lo mismo sin filtros
            scan = new Scan();
            scan.setStartRow(Bytes.toBytes("0011")).setStopRow(Bytes.toBytes("0012"));// por abajo incluye y por arriba no
            scanner = tbl.getScanner(scan);
            //TODO Mostrar el resultado
            Visualizador.PrintResult(scanner);
            //TODO Cerrar el resultado
            scanner.close();


            //TODO Liberar el objeto tabla
            tbl.close();
            //TODO Cerrar la conexión con HBase
            connection.close();

        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        System.out.println("FIN");
    }
}
