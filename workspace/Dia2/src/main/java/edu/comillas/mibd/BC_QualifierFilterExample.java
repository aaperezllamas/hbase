package edu.comillas.mibd;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class BC_QualifierFilterExample {
    public static void main(String[] args){
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        String prePathCloudera= "/home/icai/tmp/Cloudera/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        //TODO Preparación de la conexión a HBase
        Connection connection = null;
        Admin adm = null;
        try {
            //TODO Conectarse a la base de datos
            connection = ConnectionFactory.createConnection(conf);

            //TODO Obtener un objeto administrador
            adm = null;
            TableName tableName = TableName.valueOf("aap:ejemplo1");
            Table tbl = connection.getTable(tableName);
            //TODO Conectarse a la tabla 'Ejemplo1' de cada alumno

            //TODO Crear un objeto scan
            //se define un filtro donde el cualificador sea menor o igual que "matricula"
            Filter filter = new QualifierFilter(CompareFilter.CompareOp.LESS_OR_EQUAL, new BinaryComparator(Bytes.toBytes("matricula")));
            //TODO Asociar el filtro al objeto scan
            Scan scan = new Scan();
            scan.setFilter(filter);
            //TODO Obtener el resultado
            ResultScanner scanner = tbl.getScanner(scan);
            //TODO Mostrar el resultado
            Visualizador.PrintResult(scanner);
            //TODO Explicar el resultado
            // combierte el resultadoa  bytes por lo tanto lo compara, quedando las mayusculas antes que las minusculas,
            //TODO Cerrar el resultado
            scanner.close();


            //También se pueden asociar filtros a objetos get
            //TODO crear un objeto get sobre el rowKey 0002
            Get get = new Get(Bytes.toBytes("0002"));
            //TODO asociar el filtro al objeto Get
            get.setFilter(filter);
            //TODO obtener el resultado
            Result result = tbl.get(get);
            //TODO Mostrar el resultado
            Visualizador.PrintResult(result);
            //TODO Cerrar el resultado



            //TODO Liberar el objeto tabla
            tbl.close();
            //TODO Cerrar la conexión con HBase
            connection.close();

            throw new IOException();
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        System.out.println("FIN");
    }

}
