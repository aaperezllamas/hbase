package edu.comillas.mibd;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class BE_SingleColumnValueFilterExample {
    public static void main(String[] args){
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        String prePathCloudera= "/home/icai/tmp/Cloudera/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        //Preparación de la conexión a HBase
        Connection connection=null;
        Admin adm = null;

        try {
            //TODO Conectarse a la base de datos
            connection = ConnectionFactory.createConnection(conf);

            //TODO Obtener un objeto administrador
            adm = connection.getAdmin();

            Table tbl = null;
            TableName tableName = TableName.valueOf("aap:ejemplo1");
            tbl = connection.getTable(tableName);
            //TODO Conectarse a la tabla 'Ejemplo1' de cada alumno


            //TODO Crear un objeto Scan
            Scan scan = new Scan();
            //scan.addColumn(Bytes.toBytes("dv"), Bytes.toBytes("Motor"));
            //se define un filtro donde el valor NO contenga "oli, pero sólo en la columna "Motor""
            SingleColumnValueFilter filter = new SingleColumnValueFilter(Bytes.toBytes("dv"),
                    Bytes.toBytes("Motor"),
                    CompareFilter.CompareOp.NOT_EQUAL,
                    new SubstringComparator("oli"));
            //Definir el comportamiento del filtro cuando la columna no exista
            //El valor por defecto es false => row pasa si no se encuentra la columna, las que no tienen cualificador motor salen,
            // si es true y no encuentra el campo no devuelve pero si es false devuelve lo que no encuentra
            // resumen a false decuelve lo que no encuentre el campo por el que se filtra
            filter.setFilterIfMissing(false);
            //TODO Asociar el filtro al objeto scan
            scan.setFilter(filter);
            //TODO Obtener el resultado
            ResultScanner scanner = tbl.getScanner(scan);
            //TODO Mostrar el resultado
            Visualizador.PrintResult(scanner);
            //TODO Cerrar el resultado
            scanner.close();

            System.out.println("\n\n");

            //TODO Analizar el resultado del siguiente codigo
            Scan scan1 = new Scan();
            scan1.addColumn(Bytes.toBytes("dv"),Bytes.toBytes("Matricula"));
            //TODO Asociar el filtro al objeto scan
            scan1.setFilter(filter);
            //TODO Obtener el resultado
            scanner = tbl.getScanner(scan1);
            //TODO Mostrar el resultado
            Visualizador.PrintResult(scanner);
            //TODO Cerrar el resultado
            scanner.close();


            //TODO Liberar el objeto tabla
            tbl.close();
            //TODO Cerrar la conexión con HBase
            connection.close();

        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        System.out.println("FIN");
    }

}
