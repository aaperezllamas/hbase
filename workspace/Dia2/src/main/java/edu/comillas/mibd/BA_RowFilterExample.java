package edu.comillas.mibd;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class BA_RowFilterExample {
    public static void main(String[] args){
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        String prePathCloudera= "/home/icai/tmp/Cloudera/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        //TODO Preparación de la conexión a HBase
        Connection connection = null;
        Admin adm = null;

        try {
            //TODO Conectarse a la base de datos
            connection = ConnectionFactory.createConnection(conf);

            //TODO Obtener un objeto administrador
            adm = connection.getAdmin();

            Table tbl = null;
            TableName tableName = TableName.valueOf("aap:ejemplo1");
            tbl = connection.getTable(tableName);
            //TODO Conectarse a la tabla 'Ejemplo1' de cada alumno

            //Se crea un objeto scan
            Scan scan = new Scan();
            //se define un filtro donde el rowKey sea menor o igual a 0003
            Filter filter = new RowFilter(CompareFilter.CompareOp.LESS_OR_EQUAL,  new BinaryComparator(Bytes.toBytes("0003")));
            //Se asocia el filtro al objeto scan
            scan.setFilter(filter);
            //TODO obtener el resultado
            ResultScanner scanner = tbl.getScanner(scan);
            //TODO Mostrar el resultado
            Visualizador.PrintResult(scanner);
            //TODO Cerrar el resultado
            scanner.close();


            //Obtener todos los registros cuya rowKey menores de cien y que terminen en 1
            //TODO Crear un objeto scan
            filter = new RowFilter(CompareFilter.CompareOp.EQUAL, new RegexStringComparator("00\\d1$"));
            //TODO Asociar el filtro al objeto Scan
            scan = new Scan();
            scan.setFilter(filter);
            //TODO obtener el resultado
            scanner = tbl.getScanner(scan);
            //TODO Mostrar el resltado
            Visualizador.PrintResult(scanner);
            //TODO Cerrar el resultado
            scanner.close();



            //Obtener todos los registros que contengan 01 en la rowKey
            //TODO Crear un objeto scan
            filter = new RowFilter(CompareFilter.CompareOp.EQUAL, new SubstringComparator("01"));
            //TODO Asociar el filtro al objeto Scan
            scan = new Scan();
            scan.setFilter(filter);
            //TODO obtener el resultado
            scanner =  tbl.getScanner(scan);
            //TODO Mostrar el resltado
            Visualizador.PrintResult(scanner);
            //TODO Cerrar el resultado
            scanner.close();

            //TODO Liberar el objeto tabla
            tbl.close();
            //TODO Cerrar la conexión con HBase
            connection.close();

        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        System.out.println("FIN");
    }

}
