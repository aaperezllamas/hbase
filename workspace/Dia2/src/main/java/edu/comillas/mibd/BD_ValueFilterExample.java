package edu.comillas.mibd;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class BD_ValueFilterExample {
    public static void main(String[] args){
        //TODO Especificación de la configuración de HBase
        Configuration conf = HBaseConfiguration.create();
        String prePathDocker= "/home/icai/tmp/";
        String prePathCloudera= "/home/icai/tmp/Cloudera/";
        conf.addResource(new Path(prePathDocker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDocker+"core-site.xml"));

        //Preparación de la conexión a HBase
        Connection connection=null;
        Admin adm = null;

        try {
            //TODO Conectarse a la base de datos
            connection = ConnectionFactory.createConnection(conf);

            //TODO Obtener un objeto administrador
            adm = connection.getAdmin();

            Table tbl = null;
            TableName tableName = TableName.valueOf("aap:ejemplo1");
            tbl = connection.getTable(tableName);
            //TODO Conectarse a la tabla 'Ejemplo1' de cada alumno


            //TODO Crear un objeto Scan
            Scan scan = new Scan();
            ResultScanner scanner = null;
            //se define un filtro donde el valor contenga "oli"
            Filter filter = new ValueFilter(CompareFilter.CompareOp.EQUAL, new SubstringComparator("oli"));
            //TODO Asociar el filtro al objeto scan
            scan.setFilter(filter);
            //TODO Obtener el resultado
            scanner = tbl.getScanner(scan);
            //TODO Mostrar el resultado
            Visualizador.PrintResult(scanner);
            //TODO Cerrar el resultado
            scanner.close();

            //TODO Liberar el objeto tabla
            tbl.close();
            //TODO Cerrar la conexión con HBase
            connection.close();

            //TODO borrar al terminar el ejercicio
            System.out.println("Fin");

        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }

        System.out.println("FIN");
    }

}
