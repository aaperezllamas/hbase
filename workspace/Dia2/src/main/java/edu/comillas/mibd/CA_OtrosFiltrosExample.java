package edu.comillas.mibd;

import javafx.scene.control.Tab;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.MultiRowRangeFilter;
import org.apache.hadoop.hbase.filter.TimestampsFilter;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CA_OtrosFiltrosExample {
    //TODO Realizar otras consultas sobre la tabla 'Ejemplo1' de cada alumno utilizando otros filtros y comparadores no utilizados en los ejemplos
    //TODO Para ello consultar la documentación en http://hbase.apache.org/apidocs/org/apache/hadoop/hbase/filter/package-summary.html


    public static void main(String[] args){
        Configuration conf = HBaseConfiguration.create();
        String prePathDoscker = "/home/icai/tmp/";
        conf.addResource(new Path(prePathDoscker+"hbase-site.xml"));
        conf.addResource(new Path(prePathDoscker+"core-site.xml"));

        Connection connection = null;
        Admin adm = null;

        try {
            connection = ConnectionFactory.createConnection(conf);
            adm = connection.getAdmin();

            TableName tableName = TableName.valueOf("aap:ejemplo1");
            if(!adm.tableExists(tableName)){
                throw new IOException();
            }
            Table tbl = connection.getTable(tableName);

            // Filter 1 -- Timestamp filter
            Scan scan = new Scan();
            List<Long> timeStamps = new ArrayList<Long>();
            timeStamps.add(Long.valueOf(1));
            Filter filter = new TimestampsFilter(timeStamps);
            scan.setFilter(filter);
            ResultScanner scanner = tbl.getScanner(scan);
            Visualizador.PrintResult(scanner);
            scanner.close();


            // Filter 2 -- multi row range filter
            scan = new Scan();
            List<MultiRowRangeFilter.RowRange> ranges = new ArrayList<>();

            MultiRowRangeFilter.RowRange range1 = new MultiRowRangeFilter.RowRange(Bytes.toBytes("0001"), true,
                    Bytes.toBytes("0004"), false);
            MultiRowRangeFilter.RowRange range2 = new MultiRowRangeFilter.RowRange(Bytes.toBytes("0008"), false,
                    Bytes.toBytes("0013"), true);

            ranges.addAll(Arrays.asList(range1, range2));

            Filter filter2 = new MultiRowRangeFilter(ranges);
            scan.setFilter(filter);
            scanner = tbl.getScanner(scan);
            Visualizador.PrintResult(scanner);
            scanner.close();



            tbl.close();
            connection.close();

        }catch (IOException e){

        }
    }

}
